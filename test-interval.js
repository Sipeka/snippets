function initInterval(n) {
    setInterval(() => {
        console.log('interval ' + n + ' ' + new Date().toString());
    }, 500);
}

for (let i = 0; i < 10; i++) {
    initInterval(i + 1);
}
